/**
 * Created by Sam.
 */
class Book {

    constructor(name, describe){
        this.name = name;
        this.describe = describe;
        this.likes = 0;
        this.dislikes = 0;
    };

    get nameOfBook() {
        return this.name;
    }

    set nameOfBook(name) {
        this.name = name;
    }

    get describeOfBook() {
        return this.describe;
    }

    set describeOfBook (describe){
        this.describe = describe;
    }

    like(){
        this.likes++;
    };

    dislike(){
        this.dislikes++;
    };

}

export default Book;
