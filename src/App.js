import React, {Component} from 'react';
import TodoBox from './components/TodoBox';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

class App extends Component {
    render() {
        return (
            <MuiThemeProvider>
                <TodoBox/>
            </MuiThemeProvider>
        );
    }
}

export default App;
