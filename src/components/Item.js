/**
 * Created by Sam.
 */
import React, {Component} from 'react';
import {Card, CardTitle, CardText} from 'material-ui/Card';
import styles from './style.css';

class Item extends Component {

    constructor(props) {
        super(props);
        this.state = {expanded: false};
    }

    handleExpandChange = (expanded) => {
        this.setState({expanded: expanded});
    };

    render() {
        return (
            <Card expanded={this.state.expanded} onExpandChange={this.handleExpandChange}
                  className="card">
                <CardTitle
                    title={this.props.book.nameOfBook}
                    actAsExpander={true}
                    showExpandableButton={true}
                />
                <CardText expandable={true}>
                    {this.props.book.describeOfBook}
                </CardText>
            </Card>
        );
    }
}

export default Item;