/**
 * Created by Sam.
 */
import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import Card from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import Book from '../models/Book';
import styles from './style.css';

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {name: "", describe: ""};
    }

    addBook = () => {
        if (!!this.state.name && !!this.state.describe) {
            this.props.onAdd(new Book(this.state.name, this.state.describe));
            this.setState({name: "", describe: ""});
        }
    };

    onChangeName = (e) => {
        this.setState({name: e.target.value});
    };

    onChangeDescribe = (e) => {
        this.setState({describe: e.target.value});
    };

    render() {
        return (
            <Card className="card">
                <TextField
                    className="input"
                    hintText="Введите название книги"
                    onChange={this.onChangeName}
                    value={this.state.name}
                />
                <br/>
                <TextField
                    className="input"
                    hintText="Описание книги"
                    onChange={this.onChangeDescribe}
                    value={this.state.describe}
                />
                <RaisedButton
                    label="Ввести данные"
                    primary={true}
                    onTouchTap={this.addBook}
                />
            </Card>
        )
    }
}

export default Header;