/**
 * Created by Sam.
 */
import React, {Component} from 'react';
import Item from './Item';
import {List} from 'material-ui/List';
import Book from '../models/Book'
import Header from './Header';

class TodoBox extends Component {

    constructor(props) {
        super(props);
        this.state = {text: '', list: [new Book('Alice', 'big book'), new Book('Vq', 'small book')]};
    }

    addBook = (book) => {
        this.setState((prevState, props) => ({
            list: [...prevState.list, book],
        }));
    };

    render() {
        return (
            <div>
                <Header onAdd={this.addBook}/>
                <List>
                    {
                        this.state.list.map((el, i) =>
                            <Item key={i} id={'todoItem_' + i} book={el}/>
                        )
                    }
                </List>
            </div>
        );
    }

}

export default TodoBox;

